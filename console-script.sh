#!/bin/bash


# Options and prompt definitions:
OPTIONS=("Show Appliance Stats" "Show UI Login Details" "Edit/Inspect Coriolis Configuration" "Edit/Inspect Network Settings" "Edit/Inspect Proxy Settings" "Expose Coriolis Services Endpoints" "Add Certificate to Coriolis Worker" "Restore to default Coriolis Worker certificate chain" "Restart Coriolis Services")

WELCOME_PROMPT=$(cat <<EOP
Welcome to the Coriolis Appliance Interactive User Console!

EOP
)

OPTIONS_LINES=`for o in ${!OPTIONS[@]}; do printf '    %s. %s\n' "$o" "${OPTIONS[o]}"; done`
OPTIONS_PROMPT=$(cat <<EOP
Please select one of the following options:
EOP
)

CONTAINER_RESTART_PROMPT=$(cat <<EOP
Application of the latest settings requires restarting all Coriolis services.

WARNING: restarting the Coriolis containers can lead to the hanging/failure of any
currently-running operations.\n
EOP
)

EDITING_CONTAINER_CONSOLE_PROMPT_INSPECTING=$(cat <<EOP
For example, you may peform the following general maintenance actions:

    * vim /etc/coriolis/coriolis.conf       -- edit the main Coriolis configuration
    * cat /var/log/coriolis/*               -- checking Coriolis logs

After you are done, please exit this shell using exit or Ctrl^D.\n\n
EOP
)

EDITING_CONTAINER_CONSOLE_PROMPT_NETWORKING=$(cat <<EOP
For example, you may use the following to change common networking settings:

    * ip a; ping 8.8.8.8; nslookup google.com           -- check network status
    * vim /etc/netplan/50-cloud-init.yaml               -- edit network settings
    * echo 10.101.1.44 my.platform.local >> /etc/hosts  -- add host entries

After you are done editing, please exit this shell using exit or Ctrl^D.

Note that the networking settings are only applied *after* you edit the configurations and the
Coriolis services are restarted, so in order to validate them, please use the
Edit/Inspect Coriolis Configuration option.\n\n
EOP
)

EDITING_CONTAINER_CONSOLE_PROMPT_UPGRADE=$(cat <<EOP
Please feel free to edit the Docker registry/image settings by running:
    * vim /root/coriolis-docker/docker-images-config.yml

After you are done editing, please exit this shell using exit or Ctrl^D.\n\n
EOP
)

EXPOSING_CORIOLIS_SERVICES_PROMPT=$(cat <<EOP
This will expose the Coriolis services endpoints by setting them on the main IP address of the
appliance (instead of the default 127.0.0.1), thus allowing API access to external clients.

WARNING: This operation requires stopping all Coriolis services while updating endpoint configuration,
please make sure no running executions are active.\n\n
EOP
)

PROXY_SETTINGS=$(cat <<EOP
This will set-up HTTP/HTTPS/NO PROXY environment variables in coriolis-worker docker container.

WARNING: This operation requires restarting all Coriolis containers and docker service,
please make sure no running executions are active.\n\n
EOP
)

ADD_CERTIFICATE_TO_WORKER_PROMPT=$(cat <<EOP
This option will append a PEM-encoded certificate to the worker, so it will be able to connect to certain
platforms (i.e. AzureStack).\n\n
EOP
)

RESTORE_WORKER_CERTIFICATE_CHAIN=$(cat <<EOP
This option will restore the worker certificate chain to its system default. Any added certificates through this prompt
will be removed, and might need to be re-added.\n\n
EOP
)

# Source parent scripts:
BASE_DIR=$(dirname "$(readlink -f "$0")")
source "$BASE_DIR/utils/common.sh"

CORIOLIS_CONSOLE_EDITOR_CONTAINER_NAME="coriolis-console-editor"
ADMIN_OPENRC_FILE="$(get_global_config_value kolla_admin_openrc_filepath)"
CORIOLIS_CONSOLE_EDITOR_EXTRA_PROPT_FILE_PATH="/opt/coriolis/extra-coriolis-editor-prompt.txt"

# Logging options:
LOGDIR=`get_global_config_value coriolis_log_dir`
CONSOLE_SCRIPT_LOG_FILE="$LOGDIR/coriolis-console-script.log"

# General constants:
LANDSCAPE_SYSINFO_FILE_PATH=/etc/update-motd.d/50-landscape-sysinfo
TMP_WORKER_CERTIFICATE_PATH=/etc/coriolis/tmp-worker-cert.pem
DOCKER_CONTAINERS_FOLDER="/var/lib/docker/containers"

# Logs all given args to the $CONSOLE_SCRIPT_LOG_FILE
function log-console-message {
    echo "$@" >> "$CONSOLE_SCRIPT_LOG_FILE"
    echo >> "$CONSOLE_SCRIPT_LOG_FILE"
}

# Runs a command and logs it in the $CONSOLE_SCRIPT_LOG_FILE:
function run-logged-command {
    ERRSWP=`mktemp`
    log-console-message "### [$(date --iso-8601=seconds)] START COMMAND: \t$@"
    out=`bash -c "$@" 2> "$ERRSWP"`
    err=`cat $ERRSWP`
    rm $ERRSWP
    log-console-message "stdout:\n$out"
    log-console-message "stderr:\n$err\n"
    log-console-message "### [$(date --iso-8601=seconds)] END COMMAND: \t$@"
    echo "$out"
}

# Returns the names of all currently-defined Coriolis containers. (including currently stopped ones)
function get-coriolis-containers {
    echo "$(run-logged-command 'docker ps -a | awk "{print \$NF}" | grep "coriolis-*" | tr "\n" " "')"
}

# Returns the IP address on the main external interface:
function get-main-ip-address {
    echo "$(run-logged-command 'python3 -c "import os; os.chdir(\"/root/coriolis-docker\"); import expose_coriolis; print(expose_coriolis.get_main_ip())"')"
}

# Runs a shell in the coriolis-console-editor container.
function run-coriolis-console-editor-shell {
    EXTRA_PROMPT="$1"
    printf "$EXTRA_PROMPT" > "$CORIOLIS_CONSOLE_EDITOR_EXTRA_PROPT_FILE_PATH"
    log-console-message "### Entering $CORIOLIS_CONSOLE_EDITOR_CONTAINER_NAME container."
    docker exec -ti $CORIOLIS_CONSOLE_EDITOR_CONTAINER_NAME bash
}

# Prints docker-related stats:
function print-docker-status {
    printf "### docker ps -a\n"
    echo "$(run-logged-command 'docker ps -a')"
    printf "### docker stats\n"
    echo "$(run-logged-command 'docker stats --no-stream')"
}

# Prints status information about the appliance.
function print-status {
    echo "$(run-logged-command $LANDSCAPE_SYSINFO_FILE_PATH)"
    printf "### df -h\n"
    echo "$(run-logged-command 'df -h')"
    print-docker-status
}

# Prints UI login details:
function print-ui-details {
    IP=`get-main-ip-address`
    echo
    printf "URL = https://$IP\n"
    cat /etc/kolla/admin-openrc.sh | grep -E "(USERNAME|PASSWORD)" | sed -e 's/export OS_//g' -e 's/=/ = /g'
    echo
    echo "NOTE: the internal IP address shown above may not be directly accessible on some clouds."
}

# Restarts all Coriolis service containers. (including currently stopped ones)
function restart-coriolis-containers {
    echo "Restarting Coriolis Logger service."
    run-logged-command "systemctl restart coriolis-logger.service"
    if [ $? -eq 0 ]; then
        echo "Successfully restarted Coriolis Logger Service."
    fi
    echo "Restarting Coriolis Service containers."
    run-logged-command "docker restart $(get-coriolis-containers)"
    if [ $? -eq 0 ]; then
        echo "Successfully restarted Coriolis Service Containers."
    fi
}

# Prompts for y/n confirmation.
function prompt-for-confirmation-word {
    PROMPT="$1"
    while true; do
        read -p "$PROMPT (y/[n]): " input
        if [ "$input" = "y" ]; then
            echo "1"
            break
        fi

        if [ "$input" = "n" ] || [ ! "$input" ]; then
            echo "0"
            break
        fi
    done
}

# Asks for user confirmation and restart all Coriolis containers. (inclusing currently stopped ones)
function confirm-restart-coriolis-containers {
    printf "$CONTAINER_RESTART_PROMPT"
    echo
    CONFIRMED=`prompt-for-confirmation-word "Restart Coriolis containers now? "`
    if [ "$CONFIRMED" = "1" ]; then
        restart-coriolis-containers $@
    else
        echo
        echo '!!! Please remember to restart the Coriolis services for any new settings to take effect !!!'
    fi
}

function expose-coriolis-services {
    printf "$EXPOSING_CORIOLIS_SERVICES_PROMPT"
    echo
    CONFIRMED=`prompt-for-confirmation-word "Expose Coriolis Services now? "`
    if [ "$CONFIRMED" = "1" ]; then
        MAIN_ADDRESS=`get-main-ip-address`
        read -p "Which IP address should the Coriolis Services be exposed to? [$MAIN_ADDRESS]: " BIND_ADDRESS
        BIND_ADDRESS=${BIND_ADDRESS:-$MAIN_ADDRESS}
        python3 $BASE_DIR/expose_coriolis.py --use-address $BIND_ADDRESS
    fi
    echo "Sourcing ~/.bashrc"
    source ~/.bashrc
}

function add-certificate-to-worker {
    printf "$ADD_CERTIFICATE_TO_WORKER_PROMPT"
    CERTIFI_DIR=$(run-logged-command "docker exec coriolis-worker python3 -c 'import certifi; import os; print(os.path.dirname(certifi.__file__))'")
    CACERT_PEM_PATH="$CERTIFI_DIR/cacert.pem"

    read -p "Please enter a URL containing the certificate data: " URL

    echo "Downloading PEM certificate..."
    wget -O $TMP_WORKER_CERTIFICATE_PATH $URL
    if ! [ $? -eq 0 ]; then
        echo "ERROR: Failed to download the certificate from URL: $URL"
        return
    fi

    echo "Validating downloaded PEM certificate"
    openssl x509 -noout -in $TMP_WORKER_CERTIFICATE_PATH
    if ! [ $? -eq 0 ]; then
        echo "ERROR: The provided URL's contents do not contain a valid PEM certificate!"
        return
    fi

    run-logged-command "docker exec coriolis-worker bash -c 'if [ ! -f $CACERT_PEM_PATH.bak ]; then cp $CACERT_PEM_PATH $CACERT_PEM_PATH.bak; fi'"
    run-logged-command "docker exec coriolis-worker bash -c 'cat $TMP_WORKER_CERTIFICATE_PATH >> $CACERT_PEM_PATH; echo >> $CACERT_PEM_PATH'"
}

function restore-certificate-chain {
    printf "$RESTORE_WORKER_CERTIFICATE_CHAIN"
    CONFIRMED=`prompt-for-confirmation-word "Restore certificate chain now? "`
    CERTIFI_DIR=$(run-logged-command "docker exec coriolis-worker python3 -c 'import certifi; import os; print(os.path.dirname(certifi.__file__))'")
    CACERT_PEM_PATH="$CERTIFI_DIR/cacert.pem"
    if [ "$CONFIRMED" = "1" ]; then
        run-logged-command "docker exec coriolis-worker bash -c 'if [ -f $CACERT_PEM_PATH.bak ]; then cp $CACERT_PEM_PATH.bak $CACERT_PEM_PATH; fi'"
    fi
}

function display-proxy-settings {
    CURR_PROXY_SETTINGS=$(run-logged-command "docker exec -ti coriolis-worker env|grep PROXY")
    echo -e "Current Proxy settings:\n$CURR_PROXY_SETTINGS\n"
}

#checks coriolis-worker .json configuration file for env. var. existence and creates new/updates values
#$1 argument ccontains the env. variable name, $2 the value of it, $3 coriolis-worker container .json configuration file path
function apply-proxy-variable {
    VAR_PRESENT=$(run-logged-command "jq --arg value $1 '.Config.Env | contains(["'$value'"])' $2")
    if [[ $VAR_PRESENT == "true" ]]; then
        run-logged-command "jq --arg var1 $1 --arg value $1=$3 '(.Config.Env[] | select(contains("'$var1'"))) |= "'($value ? // .)'"' $2 > "$2.tmp" && mv "$2.tmp" "$2""
    else
        run-logged-command "jq --arg var1 $1 --arg value $1=$3 '(.Config.Env += ["'$value'"])' $2 > "$2.tmp" && mv "$2.tmp" "$2""
    fi
}

#127.0.0.1 and Coriolis Appliance IP@ needs to skip proxy since Coriolis internal endpoints are using it.
function add-proxy {
    printf "$PROXY_SETTINGS"
    WORKER_CONT_ID=$(run-logged-command 'docker inspect --format="{{.Id}}" coriolis-worker')
    WORKER_CONT_CONF_FILE="$DOCKER_CONTAINERS_FOLDER/$WORKER_CONT_ID/config.v2.json"
    IP_ADDR=`get-main-ip-address`

    display-proxy-settings
    CONTINUE_SETUP=`prompt-for-confirmation-word "Continue setting-up proxy environment variables? "`
    if [ "$CONTINUE_SETUP" = "0" ]; then
        echo
        return
    fi

    read -p "Enter HTTP proxy ( http://<HOST>:<PORT>, Enter=None ): " PROXY_HTTP
    read -p "Enter HTTPS proxy ( http(s)://<HOST>:<PORT>, Enter=None ): " PROXY_HTTPS
    read -p "Enter NO proxy ( *.test.example.com,.example2.com ): " PROXY_SKIP
    APPLY_PROXY_WORKER=`prompt-for-confirmation-word "Apply Proxy settings to Coriolis Worker container? "`
    if [ "$APPLY_PROXY_WORKER" = "1" ]; then
        echo
        echo 'Stopping coriolis-worker container'
        run-logged-command "docker stop coriolis-worker 2>&1 > /dev/null"
        if ! [ $? -eq 0 ]; then
            echo "ERROR: Failed to stop coriolis-worker container."
            return
        fi
        apply-proxy-variable HTTP_PROXY $WORKER_CONT_CONF_FILE $PROXY_HTTP
        apply-proxy-variable HTTPS_PROXY $WORKER_CONT_CONF_FILE $PROXY_HTTPS
        apply-proxy-variable NO_PROXY $WORKER_CONT_CONF_FILE 127.0.0.1,$IP_ADDR,$PROXY_SKIP
        echo "Restarting docker service."
        run-logged-command "systemctl restart docker"
        echo "Starting coriolis-worker container."
        run-logged-command "docker start coriolis-worker 2>&1 > /dev/null"
        if ! [ $? -eq 0 ]; then
            echo "ERROR: Failed to start coriolis-worker container."
            return
        fi
        display-proxy-settings
    else
        echo
        echo 'Proxy settings not applied to Coriolis Worker container.'
    fi
}

function interact {
    echo "$OPTIONS_PROMPT"
    PS3="Select option: "
    select opt in "${OPTIONS[@]}"; do
        case $opt in
                "Show Appliance Stats")
                        print-status | less
            break
                        ;;
                "Show UI Login Details")
                        print-ui-details
            break
                        ;;
                "Edit/Inspect Coriolis Configuration")
                        run-coriolis-console-editor-shell "$EDITING_CONTAINER_CONSOLE_PROMPT_INSPECTING"
                        confirm-restart-coriolis-containers
            break
                        ;;
                "Edit/Inspect Network Settings")
                        run-coriolis-console-editor-shell "$EDITING_CONTAINER_CONSOLE_PROMPT_NETWORKING"
                        netplan apply
                        confirm-restart-coriolis-containers
            break
                        ;;
                "Edit/Inspect Proxy Settings")
                        add-proxy
            break
                        ;;
                "Expose Coriolis Services Endpoints")
                        expose-coriolis-services
            break
                        ;;
                "Add Certificate to Coriolis Worker")
                        add-certificate-to-worker
                        confirm-restart-coriolis-containers
            break
                        ;;
                "Restore to default Coriolis Worker certificate chain")
                        restore-certificate-chain
                        confirm-restart-coriolis-containers
            break
                        ;;
                "Restart Coriolis Services")
                        confirm-restart-coriolis-containers
            break
                        ;;
                "Upgrade Coriolis Components")
                        run-coriolis-console-editor-shell "$EDITING_CONTAINER_CONSOLE_PROMPT_UPGRADE"
                        $BASE_DIR/coriolis-ansible update
            break
                        ;;
                "Open Shell")
                        /bin/bash
            break
                        ;;
                *)
                        echo "invalid option $REPLY";;
        esac
    done
}


trap interact INT

echo "$WELCOME_PROMPT"
$LANDSCAPE_SYSINFO_FILE_PATH
while true
do
    echo
    interact
done
